from pydantic import BaseModel


class OpensearchMessage(BaseModel):
    title: str = None
    body: str = None

class ConfigurationUpdate(BaseModel):
    value: str = None
