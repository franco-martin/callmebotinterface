from fastapi import FastAPI, Request, status
from fastapi.exceptions import RequestValidationError
import requests, urllib.parse, sys, os, logging
from fastapi.responses import JSONResponse
import app.datamodel as datamodel
from dotenv import load_dotenv
from fastapi.encoders import jsonable_encoder

# Disable healtcheck logging
class EndpointFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.getMessage().find("/healthcheck") == -1

# Filter out /healtcheck
logging.getLogger("uvicorn.access").addFilter(EndpointFilter())
logger = logging.getLogger("uvicorn.info")
logger.setLevel(os.getenv("LOG_LEVEL", "INFO").upper())

load_dotenv()
callmebot_url = os.getenv("CALLMEBOT_URL", default="https://api.callmebot.com/whatsapp.php")
callmebot_apikey = os.getenv("CALLMEBOT_APIKEY")
phone_number = os.getenv("PHONE_NUMBER")
configuration = {
    'log_request': "1", 
}

app = FastAPI()

@app.get("/healthcheck")
async def healthcheck():
    return {"message": "All good brotha!"}

@app.post("/config/{key}")
async def config(key, item: datamodel.ConfigurationUpdate):
    if key in configuration.keys():
        configuration[key]=item.value
        return {"message": f"Updated configuration {key} to {item.value}"}
    else:
        return {"message": "They key doesnt exist"}

@app.get("/config")
async def config():
    return {"data": configuration}


@app.post("/opensearch")
async def sendMessage(item: datamodel.OpensearchMessage):
    encoded_body = urllib.parse.urlencode({'text': f'*{item.title}* \n{item.body}'})
    response = requests.get(f'{callmebot_url}?phone={phone_number}&{encoded_body}&apikey={callmebot_apikey}')
    # TODO validate response
    logger.info(f'response: {response.status_code} - {response.text}')
    return {"message": "Notified whatsapp"}

@app.post("/opensearch-text")
async def sendMessage(request: Request):
    body = await request.body()
    body = body.decode(encoding='utf-8')
    logger.debug(f"Processing body: '{body}'")
    encoded_body = urllib.parse.urlencode({'text': f'{body}'})
    response = requests.get(f'{callmebot_url}?phone={phone_number}&{encoded_body}&apikey={callmebot_apikey}')
    # TODO validate response
    logger.info(f'response: {response.status_code} - {response.text}')
    return {"message": "Notified whatsapp"}

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    logger.exception("Error processing message")
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": exc.errors(), "body": exc.body}
    )